import React from 'react';
import data from 'data/candidates.json';
import Search from 'components/Search';

function App() {
  return (
    <section>
      <Search candidates={data} />
    </section>
  );
}

export default App;
