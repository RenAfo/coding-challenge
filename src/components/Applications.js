import React, { Fragment, useState } from "react";
import { Modal, Title, Body } from "ui-kit/Modal";

function Applications(props) {
  const { application, visible, name } = props;
  const [modalOpen, setModal] = useState(false);

  let changeModal = e => {
    setModal(!modalOpen);
  }

  return (
    <Fragment>
      <div key={application.id} className="application-table">
        <div className={visible ? "visible pointer" : "invisible pointer"} onClick={changeModal}>
          {application.role.title}
        </div>
        <div className={visible ? "visible pointer" : "invisible pointer"} onClick={changeModal}>
          {application.new_status.label}
        </div>
      </div>
      <Modal isOpen={modalOpen} onClose={changeModal}>
        <Title>
          <h1>{name}</h1>
        </Title>
        <Body>
          <h3>Role: {application.role.title}</h3>
          <h4>Application status: {application.new_status.label}</h4>
        </Body>
      </Modal>
    </Fragment>
  )
}

export default Applications;
