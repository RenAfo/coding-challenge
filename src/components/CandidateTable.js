import React from "react";
import CandidateDetails from "./CandidateDetails";

function CandidateTable(props) {
  const candidateList = props;

  return (
    <div key="main-table" className="main-table">
      <div>Candidate Name</div>
      <div>Status</div>
      <div># Apps</div>
      <div>Last Action</div>
      {candidateList.candidates.map((candidateDetail) => {
        return <CandidateDetails key={candidateDetail.id} candidate={candidateDetail} />
      })}
    </div>
  )
}

export default CandidateTable;
