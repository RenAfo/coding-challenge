import React, { useState } from "react";
import CandidateTable from "./CandidateTable";

function Search(props) {
  const candidates = props.candidates.results;
  const totalCandidates = props.candidates.count;
  const [searchField, setSearchField] = useState("");

  const filteredCandidates = candidates.filter(
    candidate => {
      return(
        candidate
          .name
          .toLowerCase()
          .includes(searchField.toLowerCase()) ||
        candidate
          .email
          .toLowerCase()
          .includes(searchField.toLowerCase())
      )
    }
  );

  const handleChange = e => {
    setSearchField(e.target.value);
  };

  function total() {
    return (
      <div>
        {totalCandidates} Candidates
      </div>
    );
  }

  function candidateTable() {
    return (
      <CandidateTable candidates={filteredCandidates} />
    )
  }

  return (
    <section className="main">
      {total()}
      <div>
        <input type="search" placeholder="Search by name or email" onChange={handleChange} />
      </div>
      {candidateTable()}
    </section>
  )
}

export default Search;
