import React, { Fragment, useState } from "react";
import Applications from "./Applications";
import moment from "moment";
import Minus from "ui-kit/icons/svg/icon-minus-with-circle.svg";
import Plus from "ui-kit/icons/svg/icon-plus-with-circle.svg";

function CandidateDetails(props) {
  const candidate = props.candidate;
  const [visible, setVisible] = useState(false);
  const sortedApplications = candidate.applications.sort((a, b) => a.updated - b.updated);

  let setVisibility = e => {
    setVisible(!visible);
  }

  return (
    <Fragment>
      <div key={candidate.id} id={candidate.id} className="candidate-details-map">
        <div className="pointer" onClick={setVisibility}>{candidate.name}</div>
        <div className="pointer" onClick={setVisibility}>{sortedApplications[0].new_status.label}</div>
        <div className="pointer" onClick={setVisibility}>{candidate.applications.length}</div>
        <div className="pointer" onClick={setVisibility}>{moment(candidate.profile.updated).fromNow()}</div>
        <div className="pointer" onClick={setVisibility}>
          <img src={visible ? Minus : Plus } alt={visible ? "expand" : "collapse"}/>
        </div>
        <div className="applications">
          {candidate.applications.map(application => {
            return (
              <Applications application={application} visible={visible} name={candidate.name}/>
            )
          })}
        </div>
      </div>
    </Fragment>
  )
}

export default CandidateDetails;
